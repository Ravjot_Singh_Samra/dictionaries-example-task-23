﻿using System;
using System.Collections.Generic;

namespace dictionaries_example_task_23
{
    class Program
    {
        static void Main(string[] args)
        {
            var movies = new Dictionary<string, string>();
            movies.Add("Horror movie", "Horror");
            movies.Add("Comedy movie", "Comedy");
            movies.Add("Action movie", "Action");
            movies.Add("Horror movie 2", "Horror");
            movies.Add("Triumph of the Will", "Propaganda");

            foreach (var Comedy in movies)
            {
                Console.WriteLine(Comedy.Key);
                Console.WriteLine(Comedy.Value);
            }
        }
    }
}
